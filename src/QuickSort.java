public class QuickSort {

    public static void quicSort(int[] array, int size){

        if(size > 1) {
            int dzielnik = array[size-1];
            int [] mniejsza = new int [size];
            int [] wieksza = new int [size];
            int k =0;
            int l =0;
            for (int i = 0; i <size ; i++) {
                int wartosc = array[i];
                if(wartosc < dzielnik){
                    mniejsza[k] = wartosc;
                    k++;
                }
                if (wartosc > dzielnik) {
                    wieksza[l] = wartosc;
                    l++;
                }
            }
            quicSort(mniejsza, k);
            quicSort(wieksza, l);
        }
        else System.out.println(array[0]);
    }
}
