public class CounterSort {

    public static void counterSort(int[] array) {
        int max = array[0];
        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }
        int[] iloscWystapienDodatnich = null;
        int[] iloscWystapienUjemnych = null;
        if (max > 0) {
            iloscWystapienDodatnich = new int[max - min + 1];
        }
        if (min < 0) {
            iloscWystapienUjemnych = new int[max - min + 1];
        }

        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            if (value >= 0) {
                iloscWystapienDodatnich[value] = iloscWystapienDodatnich[value] + 1;
            }
            if (value < 0) {
                value = Math.abs(value);
                iloscWystapienUjemnych[value] = iloscWystapienUjemnych[value] + 1;
            }
        }
        int counter = 0;
        if (iloscWystapienUjemnych.length > 0) {
            for (int i = (iloscWystapienUjemnych.length -1); i > -1; i--) {
                for (int j = 0; j <iloscWystapienUjemnych[i] ; j++) {
                    array[counter] = -i;
                    counter++;
                }
            }
        }
        if(iloscWystapienDodatnich.length > 0) {
            for (int i = 0; i <iloscWystapienDodatnich.length ; i++) {
                for (int j = 0; j <iloscWystapienDodatnich[i] ; j++) {
                    array[counter] = i;
                    counter++;
                }
            }
        }
    }
}
